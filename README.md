# File Time Date Converter
This application will allow you to drag a selection of files into a drag drop box, create a list of text in the 'Data' column with info about each data cell in the 'Description' column. 

You can use anything in the data column to rename a bunch of files with the time and date that they were created and output them in a folder path of your choosing in the yellow form.

Help info is found directly in the application.
